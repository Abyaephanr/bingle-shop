'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Orders extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Users, {foreignKey: 'item_id'})
      this.belongsTo(models.Users, {foreignKey: 'user_id'})
    }
  }
  Orders.init({
    item_id: DataTypes.INTEGER,
    user_id: DataTypes.INTEGER,
    status_order: DataTypes.STRING,
    total_order: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Orders',
  });
  return Orders;
};
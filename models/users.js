'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Users.init({
    email_user: DataTypes.STRING,
    password: DataTypes.STRING,
    name_user: DataTypes.STRING,
    address_user: DataTypes.STRING,
    contact_user: DataTypes.STRING,
    age_user: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Users',
  });
  return Users;
};
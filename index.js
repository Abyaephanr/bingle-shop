const express = require('express')
const app = express()
const { Users, Items, Orders } = require('./models')

app.use(express.json())
app.use(express.urlencoded({ extends: false }))

//API Register User
app.post("/user/register", (req,res) => {
    try {
        const email_user = req.body.email_user
        const password = req.body.password
        const name_user = req.body.name_user
        const address_user = req.body.address_user
        const contact_user = req.body.contact_user
        const age_user = req.body.age_user

        Users.create({
            email_user: email_user,
            password: password,
            name_user: name_user,
            address_user: address_user,
            contact_user: contact_user,
            age_user: age_user
        }).then(response => {
            // console.log(response)
            return res.json(response)
        })

    } catch (error) {
        // console.log(error)
        return req.status(500).json(`server error: ${error}`)
    }
})

//API Login User
app.post("/user/login", async (req,res,next) => {
    try {
        const loginUser = await Users.findOne({
            where: {email_user:req.body.email_user}
        })
        if (loginUser.password == req.body.password) {
            return res.status(200).json(`Berhasil Login, Selamat Datang ${loginUser.name_user}`)
        } else {
            return res.status(200).json(`Gagal Login, pastikan email dan password benar`)
        }

    } catch (error) {
        console.log(error)
        return res.status(500).json(`server error: ${error}`)
    }
})

//API Edit/Update User
app.post("/user/update", (req,res) => {
    try {
        Users.update({
            password: req.body.password_baru,
            address_user: req.body.address_user_baru,
            contact_user: req.body.contact_user_baru,
        },
        {
            where : {email_user : req.body.email_user}
        }).then(response => {
            // console.log(response)
            return res.json(response)
        })

    } catch (error) {
        // console.log(error)
        return req.status(500).json(`server error: ${error}`)
    }
})

//API tambah item
app.post("/item/tambah", (req,res) => {
    try {
        const name_item = req.body.name_item
        const price_item = req.body.price_item
        const quantity_item = req.body.quantity_item
        const descritpion_item = req.body.descritpion_item

        Items.create({
            user_id: req.body.user_id,
            name_item: name_item,
            price_item: price_item,
            quantity_item: quantity_item,
            descritpion_item: descritpion_item
        }).then(response => {
            // console.log(response)
            return res.json(response)
        })

    } catch (error) {
        // console.log(error)
        return req.status(500).json(`server error: ${error}`)
    }
})

//API Delete Item
app.post("/item/delete", (req,res) => {
    try {
        Items.destroy({
            where : {name_item : req.body.name_item}
        }).then(() => {
            return res.status(200).json(`Item ${req.body.name_item} telah dihapus`)
        })

    } catch (error) {
        console.log(error)
        return req.status(500).json(`server error: ${error}`)
    }
})

//API Melihat semua item
app.get("/item/listitems", (req, res) => {
    try {
        Items.findAll({
            include: [{
                model: Users
            }]
        })
        .then(items => {
            console.log(items)
            return res.status(200).json(items)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json(error)
    }
})

//API Melihat data item spesifik
app.get("/spesificitem/:user_id", (req, res) => {
    try {
        Items.findAll({
            where: {user_id: req.params.user_id},
            include: [{
                model: Users
            }]
        })
        .then(items => {
            console.log(items)
            return res.status(200).json(items)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json(error)
    }

})


//API Edit/Update Item
app.post("/item/update", (req,res) => {
    try {
        Items.update({
            user_id: req.body.user_id_baru,
            quantity_item: req.body.quantity_item_baru,
            descritpion_item: req.body.descritpion_item_baru
        },
        {
            where : {name_item : req.body.name_item}
        }).then(response => {
            // console.log(response)
            return res.json(response)
        })

    } catch (error) {
        // console.log(error)
        return req.status(500).json(`server error: ${error}`)
    }
})

//API Create order
app.post("/order/create", (req,res) => {
    try {
        const status_order = req.body.status_order
        const total_order = req.body.total_order

        Orders.create({
            item_id: req.body.item_id,
            user_id: req.body.user_id,
            status_order: status_order,
            total_order: total_order
        }).then(response => {
            // console.log(response)
            return res.json(response)
        })

    } catch (error) {
        // console.log(error)
        return res.status(500).json(`server error: ${error}`)
    }
})


app.listen(5000, () => {
    console.log("Server Running on Locallhost port:5000")
})
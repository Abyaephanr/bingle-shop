'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email_user: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      name_user: {
        type: Sequelize.STRING
      },
      address_user: {
        type: Sequelize.STRING
      },
      contact_user: {
        type: Sequelize.STRING
      },
      age_user: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Users');
  }
};